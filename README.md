# arcenciel

Arcenciel modify the light of your mice according the use of your RAM


# Installation

Only if using systemd

```
git clone https://gitlab.com/zilot.r/arcenciel.git
cd arcenciel/
sudo ./install.sh
```


# Uninstallation

Run the script uninstall.sh as root or via sudo :

```
sudo ./uninstall.sh
```
