"""
License :

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

Author :
	Zilot

"""

import os

# colors to use according the % mem, change to whatever you want (RGB)
colors = {
	0 : "7700FF",
	10 : "0000FF",
	20 : "0000FF",
	30 : "0000FF",
	40 : "0077FF",
	50 : "00FF77",
	60 : "00FF00",
	70 : "FFFF00",
	80 : "FF7700",
	90 : "FF0000"
    }



# get memory data
tot_m, used_m, free_m = map(int, os.popen('free -t -m').readlines()[-1].split()[1:])

# get % used and wich segment it is (by 10)
perc_used = used_m / tot_m * 100
mem_index = int(perc_used/10) * 10

# get device name using ratbagctl & some formating
device = os.system("ratbagctl list | cut -d':' -f2 | tr -s ' ' | sed 's/^.//'")

# Synch colors using ratbagctl
os.system(f"ratbagctl '{device}' led 0 set color {colors[mem_index]}")




