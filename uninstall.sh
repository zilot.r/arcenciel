#!/bin/sh

if [ "$USER" != "root" ]
then
    echo "This uninstall script must be run as root. Try : "
    echo "    sudo ./uninstall.sh"
    exit
fi

dest_file="/usr/bin/arcenciel.py"

    echo "Removing $dest_file"
    sudo rm $dest_file
    sudo cp arcenciel.py $dest_file

    echo "Stoping service"
    systemctl disable arcenciel.timer
    systemctl disable arcenciel.service
    systemctl stop arcenciel.timer
    systemctl stop arcenciel.service

    echo "Removing systemctl files from /etc/systemd/system"
    rm /etc/systemd/system/arcenciel.service
    rm /etc/systemd/system/arcenciel.timer


