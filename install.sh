#!/bin/sh

if [ "$USER" != "root" ]
then
    echo "This installation script must be run as root. Try : "
    echo "    sudo ./install.sh"
    exit
fi

dest_file="/usr/bin/arcenciel.py"

# check if file already exists in dest directoty
if [ -f "$bin_dir" ]
then

    # ask the user if it's still okay to install it, if no we exist the script
    while [ "$answer" != "Y" ]
    do
        read -p "The file $dest_file already exists. Are you sure you want to proceed ? (Y/n) " answer

        # upperase the answer
        answer=`echo "$answer" |tr a-z A-Z`

        if [ $answer == 'N' ]
        then
            exit
        elif [ $answer != 'Y' ]
        then
            echo "Wrong input, answer with 'Y' or 'N'"
        fi

    done
fi

    echo "Moving arcenciel.py to $dest_file"
    sudo cp arcenciel.py $dest_file

    echo "Adding systemctl files to /etc/systemd/system"
    cp arcenciel.service /etc/systemd/system/
    cp arcenciel.timer /etc/systemd/system/

    sudo systemctl daemon-reload
    systemctl enable arcenciel.timer
    systemctl enable arcenciel.service
    systemctl start arcenciel.service
    systemctl start arcenciel.timer


